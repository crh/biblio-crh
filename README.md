[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
[![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)


# Bibliographie du Centre de recherches historiques

## A propos

Le Centre de recherches historiques (CRH, UMR 8558) est un laboratoire du Centre national de la recherche scientifique (CNRS) et de l'École des hautes études en sciences socciales (EHESS), fondé en 1947 par Fernand Braudel. Ses chercheurs travaillent sur des sujets allant du Moyen Âge jusqu'à la période contemporaine et sur des aires culturelles variées.

Ce dossier contient la bibliographie de l'ensemble des membres du CRH depuis 1947, présentée par année au format BibLaTeX. Il s'agit d'une réalisation du groupe de travail « Bibliographie » du collectif « Sources et données de la recherche » du CRH : Jean-Damien Généro (CNRS), Cécile Soudan (CNRS) et Francine Filoche (EHESS).

Le travail est toujours en cours.

## Fichiers

Les fichiers de la période 1947-1997 (`/quinqua-biblio`) ont été constitués à partir du tableur Excel construit par Cécile Dauphin (CNRS), Jean-Daniel Gronoff (EHESS) et Raymonde Karp (EHESS) en 1999, à l'occasion du cinquantenaire du CRH ([lire leur article à ce sujet](https://doi.org/10.4000/ccrh.3053)). 

Les fichiers de la période 1998-2020 (`/xxxi-biblio`) ont été constitués par le groupe de travail "Bibliographie".

## Licence

<!--L'ensemble des fichiers et données est placé sous les termes de la licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International ([CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)).-->

L'ensemble des fichiers et données de ce dépôt est placé sous les termes de la licence ouverte 2.0 ([texte français](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf) | [english text](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf))

## Contact

Contact : gestion.sourcesetdonnees [at] ehess [.] fr
