#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation (en) : French National Centre for Scientific Research, Center for Historical Studies
Affiliation (fr) : Centre national de la recherche scientifique (CNRS), Centre de recherches historiques (CRH, UMR 8558)
Date : 2021-05-27
Update : 2021-05-27
"""

import os


# Document class
from bib_refs_xtraction import reference
# 2021 typologies
from bib_refs_xtraction import book_ls, chapitre_ls, proceedings_ls, article_ls, edition_ls, dict_entry_ls, preface_ls, compte_rendu_ls, trad_ls, autres_ls
# 1997 typologies
from bib_refs_xtraction import inproceedings_ls, exposition_ls, melanges_ls, art_in_collectif_ls, intro_ls, report_ls, new_edition_ls, outil_ls, thesis_ls, art_rapport_ls, conference_ls


def making_bib_files(filename, biblist):
    """
    Writting a BibLaTeX.
    :param filename: name of the BibLaTeX file.
    :type filename: str
    :param biblist: list of bib references.
    :type biblist: list
    """
    with open(os.path.join("./pdf-retro-bib", filename + ".bib"), 'w', encoding='utf-8') as biblio_file:
        for item in biblist:
            biblio_file.write('{}\n'.format(item))


# MAKING GLOBAL FILE
biblio_retro_all = [article_ls, book_ls, inproceedings_ls, dict_entry_ls, proceedings_ls, exposition_ls, melanges_ls, art_in_collectif_ls, intro_ls, preface_ls, edition_ls, report_ls, trad_ls, new_edition_ls, outil_ls, thesis_ls, art_rapport_ls, compte_rendu_ls, conference_ls]
with open('./retro-biblio/biblio-retro-all.bib', 'w', encoding='utf-8') as bibfile:
    for ls_biblio in biblio_retro_all:
        for item in ls_biblio:
            bibfile.write('{}\n'.format(item))


# MAKING TYPOLOGICAL FILES (1997)
biblio_names = ["articles_revues", "livres", "articles_actes_colloque", "notices_dictionnaires", "dir_ouvrages_collectifs", "catalogues_exposition", "melanges", "art_ouvrages_collectifs", "intro_presentation_conclusion", "prefaces_postfaces_edito", "editions_textes", "rapports", "traductions", "reeditions", "outils_recherche", "theses", "article_rapport", "compte_rendu", "conferences"]
# making a dict by merging biblio_names and biblio_retro_all
# note that biblio_retro_all is made out of lists hence both biblio_names and biblio_retro_all have the same lenght
# dict_ls = '<full bib ref [from biblio_retro_all]>': '<CRH type [from biblio_names]>'
dict_ls = {biblio_names[item]: biblio_retro_all[item] for item in range(len(biblio_names))}
"""for ls_biblio in dict_ls:
    with open(os.path.join("./retro-biblio/", str(ls_biblio) + ".bib"), 'w', encoding='utf-8') as biblio_file:
        for item in dict_ls[ls_biblio]:
            biblio_file.write('{}\n'.format(item))"""


# MAKING TYPOLOGICAL FILES (2021)
making_bib_files("monographies", book_ls)
making_bib_files("chapitres", chapitre_ls)
making_bib_files("dirouvrages", proceedings_ls)
making_bib_files("articles", article_ls)
making_bib_files("editions", edition_ls)
making_bib_files("dict", dict_entry_ls)
making_bib_files("prefpostface", preface_ls)
making_bib_files("compterendu", compte_rendu_ls)
making_bib_files("translations", trad_ls)
making_bib_files("others", autres_ls)


# MAKING ANNUAL FILES
sorted_all_date = sorted(set([Date for Date in reference.date]))
for year in sorted_all_date:
    annual_list = []
    for doc_type in biblio_retro_all:
        for doc in doc_type:
            if "year = {}".format(year) in doc:
                annual_list.append(doc)
    current_file = os.path.join("./annual-bibliography/", year + "-bibliography" + ".bib")
    with open(current_file, 'w', encoding="utf-8") as file:
        for document in annual_list:
            file.write("{}\n\n".format(document))
    print("{} ---> Done !".format(os.path.join("./annual-bibliography", year + "-bibliography" + ".bib")))

"""subprocess.call(["rm", "./annual-bibliography/Date 17-bibliography.bib"])
print("./annual-bibliography/Date 17-bibliography.bib ---> Deleted")"""
