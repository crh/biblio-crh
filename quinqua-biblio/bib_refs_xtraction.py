#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation (en) : French National Centre for Scientific Research, Center for Historical Studies
Affiliation (fr) : Centre national de la recherche scientifique (CNRS), Centre de recherches historiques (CRH, UMR 8558)
Date : 2021-05-27
Update : 2021-05-27
"""

from parsing_csv import Document


reference = Document('./publications_retrospectives.csv')
chapitre_ls = []
article_ls = []
book_ls = []
inproceedings_ls = []
dict_entry_ls = []
proceedings_ls = []
exposition_ls = []
melanges_ls = []
art_in_collectif_ls = []
intro_ls = []
intro_preface_ls = []
intro_book_ls = []
intro_journal_ls = []
preface_ls = []
preface_journal_ls = []
preface_book_ls = []
edition_ls = []
report_ls = []
trad_ls = []
new_edition_ls = []
outil_ls = []
thesis_ls = []
art_rapport_ls = []
compte_rendu_ls = []
conference_ls = []
sorted_all_date = sorted(set([Date for Date in reference.date]))
# print(sorted_all_date)


for ID, Name1, Name2, Firstname, Signatures, CoCRH, CoEXT, Type, Title, Journal, Editor, Booktitle, Preface, Dedicataire, Dict_title, Date, Location, Publisher, Series, Organization, Language, Pages \
        in zip(reference.id, reference.name1, reference.name2, reference.firstname, reference.signatures, reference.coauthorCRH, reference.coauthorEXT, reference.type, reference.title, reference.journal, reference.editor, reference.booktitle, reference.preface, reference.dedicataire, reference.dict_title, reference.date, reference.location, reference.publisher, reference.series, reference.organization, reference.language, reference.page):
    if Name2 != "":
        Name = Name1.title() + '-' + Name2.title() + ', ' + Firstname
    else:
        Name = Name1.title() + ', ' + Firstname
    Name1 = Name1.title().replace(" ", "") # pour les clefs des entrées
    CoCRH = CoCRH.replace(" et ", " and ")
    CoCRH = CoCRH.replace(" & ", " and ")
    CoCRH = CoCRH.replace(", ", " and ")
    CoEXT = CoEXT.replace(" et ", " and ")
    CoEXT = CoEXT.replace(" & ", " and ")
    CoEXT = CoEXT.replace(", ", " and ")
    Editor = Editor.title().replace(" Et ", " and ")
    Editor = Editor.replace(", ", " and ")
    Editor = Editor.replace(" & ", " and ")
    Journal = Journal.replace(" & ", " \\& ")
    Title = Title.replace(" & ", " \\& ")
    Publisher = Publisher.replace(" & ", " \\& ")
    if Signatures != "1":
        if CoCRH != "" and CoEXT != "":
            authors = Name + ' and ' + CoCRH + ' and ' + CoEXT
        elif CoCRH == "" and CoEXT != "":
            authors = Name + ' and ' + CoEXT
        elif CoEXT == "" and CoCRH != "":
            authors = Name + ' and ' + CoCRH
    else:
        authors = Name

    # MONOGRAPHIES
    if Type == "livre":
        book = f"""@book{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    publisher = {{{Publisher}}},
    series = {{{Series}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{book-crh, crh-{Date}}}
}}"""
        book_ls.append(book)
    elif Type == "réédition d'un livre":
        new_ed = f"""@book{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    publisher = {{{Publisher}}},
    series = {{{Series}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    addendum = {{réédition}},
    keywords = {{reedition-crh, crh-{Date}}}
}}"""
        book_ls.append(new_ed)

    # CHAPITRES
    elif Type == "contribution dans un livre collectif":
        art_in_collectif = f"""@incollection{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Booktitle}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{chap, collectif-crh, crh-{Date}}}
}}"""
        chapitre_ls.append(art_in_collectif)
        art_in_collectif_ls.append(art_in_collectif)  # reference to the original 1997 typology
    elif Type == "article dans des actes de colloque":
        inproceedings = f"""@inproceedings{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Booktitle}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{chap, chapitre, crh-{Date}}}
}}"""
        chapitre_ls.append(inproceedings)
        inproceedings_ls.append(inproceedings)  # reference to the original 1997 typology
    elif Type == "contribution dans des Mélanges":
        melanges = f"""@incollection{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Booktitle}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{chap, melanges-crh, crh-{Date}}}
}}"""
        # addendum = {{{Dedicataire}}},
        chapitre_ls.append(melanges)
        melanges_ls.append(melanges)  # reference to the original 1997 typology
    elif Type == "introduction":  # introduction, présentation, conclusion
        if Journal == "" and Booktitle != "" and Preface == "":
            intro_book = f"""@incollection{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Booktitle}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{chap, intro-book-crh, crh-{Date}}}
}}"""
            chapitre_ls.append(intro_book)
            intro_book_ls.append(intro_book)  # reference to the original 1997 typology
        elif Journal == "" and Booktitle == "" and Preface != "":
            intro_preface = f"""@incollection{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Preface}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{chap, intro-preface-crh, crh-{Date}}}
}}"""
            chapitre_ls.append(intro_preface)
            intro_preface_ls.append(intro_preface)  # reference to the original 1997 typology

    # ARTICLES
            chapitre_ls.append(intro_preface)
        elif Journal != "" and Booktitle == "" and Preface == "":
            intro_journal = f"""@article{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    journal = {{{Journal}}},
    year = {Date},
    language = {{{Language}}},
    keywords = {{article, intro-journal-crh, crh-{Date}}}
}}"""
            article_ls.append(intro_journal)
            intro_journal_ls.append(intro_journal)  # reference to the original 1997 typology
        else:
            # print("error for {}.\nJournal = +{}+\nBooktitle = +{}+\nPreface = +{}+\n\n".format(ID, Journal, Booktitle, Preface))
            intro_journal = f"""@article{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    journal = {{{Journal}}},
    issuetitle = {{{Preface}}},
    year = {Date},
    language = {{{Language}}},
    keywords = {{article, intro-journal-crh, crh-{Date}}}
}}"""
            article_ls.append(intro_journal)

    # ARTICLE
    elif Type == "article dans une revue":
        article = f"""@article{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    journal = {{{Journal}}},
    year = {Date},
    language = {{{Language}}},
    keywords = {{article, art-crh, crh-{Date}}}
 }}"""
        article_ls.append(article)

    # DIRECTIONS D'OUVRAGE
    elif Type == "direction d'un livre collectif":
        proceedings = f"""@proceedings{{{Name1}{ID}-{Date},
    editor = {{{authors}}},
    editortype = {{dir.}},
    title = {{{Title}}},
    booktitle = {{{Title}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{dirouvrage, proceedings-crh, crh-{Date}}}
}}"""
        proceedings_ls.append(proceedings)

    # EDITIONS
    elif Type == "édition de texte":
        if Preface != "":
            edition = f"""@book{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Preface}}},
    publisher = {{{Publisher}}},
    series = {{{Series}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    % addendum = {{{Title}}},
    keywords = {{editions, edition-txt-crh, crh-{Date}}}
}}"""
            edition_ls.append(edition)
        else:
            edition = f"""@book{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    publisher = {{{Publisher}}},
    series = {{{Series}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{editions, edition-txt-crh, crh-{Date}}}
}}"""
            edition_ls.append(edition)

    # PRÉFACE, POSTFACE, ÉDITO
    elif Type == "préface":  # préface, postface, éditorial
        # DANS UNE REVUE > ARTICLE
        if Journal != "" and Preface == "" and Booktitle == "":
            preface_journal = f"""@article{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    journal = {{{Journal}}},
    issuetitle = {{{Preface}}},
    year = {Date},
    language = {{{Language}}},
    keywords = {{preface, preface-journal-crh, crh-{Date}}}
}}"""
            preface_journal_ls.append(preface_journal)
        # DANS UN OUVRAGE > CHAPITRE
        elif Journal == "" and Preface != "" and Booktitle == "":
            preface_book = f"""@incollection{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Preface}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{preface, preface-book-crh, crh-{Date}}}
}}"""
            preface_book_ls.append(preface_book)
        # DANS UN OUVRAGE > CHAPITRE
        elif Journal == "" and Preface == "" and Booktitle != "":
            preface_book = f"""@incollection{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Booktitle}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{preface, preface-book-crh, crh-{Date}}}
}}"""
            preface_book_ls.append(preface_book)
        else:
            print("Error for Preface in {}".format(ID))

    # ENTRÉE DE DICT
    elif Type == "article de dictionnaire":
        dict_entry = f"""@inreference{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Dict_title}}},
    publisher = {{{Publisher}}},
    series = {{{Series}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{dict-crh, crh-{Date}}}
}}"""
        dict_entry_ls.append(dict_entry)

    # COMTPE RENDU
    elif Type == "compte-rendu":
        if Journal != "":
            compte_rendu = f"""@article{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    journal = {{{Journal}}},
    year = {Date},
    language = {{{Language}}},
    keywords = {{cr-crh, crh-{Date}}}
}}"""
            compte_rendu_ls.append(compte_rendu)
        else:
            compte_rendu = f"""@misc{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    year = {Date},
    language = {{{Language}}},
    addendum = {{compte-rendu}},
    keywords = {{cr-crh, crh-{Date}}}
}}"""
            compte_rendu_ls.append(compte_rendu)

    # TRADUCTIONS
    elif Type == "traduction":
        trad = f"""@book{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    publisher = {{{Publisher}}},
    series = {{{Series}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{traduction-crh, crh-{Date}}}
}}"""
        trad_ls.append(trad)

    # AUTRES
    # AUTRES > CONFÉRENCE
    elif Type == "conférence":
        conference = f"""@misc{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    year = {Date},
    language = {{{Language}}},
    keywords = {{autres, conf-crh, crh-{Date}}}
}}"""
        conference_ls.append(conference)  # reference to the original 1997 typology
    # AUTRES > THÈSES
    elif Type == "thèse":
        thesis = f"""@thesis{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    institution = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{autres, theses-crh, crh-{Date}}}
}}"""
        thesis_ls.append(thesis)  # reference to the original 1997 typology
    # AUTRES > OUTILS POUR LA RECHERCHES
    elif Type == "outil pour la recherche":
        # CHAPITRE
        if Booktitle != "":
            outil = f"""@incollection{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Booktitle}}},
    publisher = {{{Publisher}}},
    series = {{{Series}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{autres, outil-recherche-crh, crh-{Date}}}
}}"""
            outil_ls.append(outil)  # reference to the original 1997 typology
        # MONOGRAPHIES
        else:
            outil = f"""@book{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    publisher = {{{Publisher}}},
    series = {{{Series}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{autres, outil-recherche-crh, crh-{Date}}}
}}"""
            outil_ls.append(outil)  # reference to the original 1997 typology
    # AUTRES > RAPPORTS
    elif Type == "rapport":
        report = f"""@report{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    title = {{{Title}}},
    publisher = {{{Publisher}}},
    institution = {{{Organization}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{autres, report-crh, crh-{Date}}}
}}"""
        report_ls.append(report)  # reference to the original 1997 typology
    # AUTRES > CATALOGUE D'EXPO
    elif Type == "catalogue d'exposition":
        exposition = f"""@collection{{{Name1}{ID}-{Date},
    author = {{{authors}}},
    editor = {{{Editor}}},
    title = {{{Title}}},
    booktitle = {{{Booktitle}}},
    publisher = {{{Publisher}}},
    location = {{{Location}}},
    year = {Date},
    pagetotal = {{{Pages}}},
    language = {{{Language}}},
    keywords = {{autres, expo-crh, crh-{Date}}}
}}"""
        exposition_ls.append(exposition)  # reference to the original 1997 typology
    # AUTRES > ARTICLES DANS UN RAPPORT > CHAPITRE ET ARTICLE
    elif Type == "article dans un rapport":
        if Booktitle != "" and Journal == "":
            art_rapport = f"""@incollection{{{Name1}{ID}-{Date},
        author = {{{authors}}},
        editor = {{{Editor}}},
        title = {{{Title}}},
        booktitle = {{{Booktitle}}},
        publisher = {{{Publisher}}},
        series = {{{Series}}},
        location = {{{Location}}},
        year = {Date},
        pagetotal = {{{Pages}}},
        language = {{{Language}}},
        keywords = {{autres, art-rapport-crh, crh-{Date}}}
        }}"""
            art_rapport_ls.append(art_rapport)  # reference to the original 1997 typology
        elif Booktitle == "" and Journal != "":
            art_rapport = f"""@article{{{Name1}{ID}-{Date},
        author = {{{authors}}},
        title = {{{Title}}},
        journal = {{{Journal}}},
        year = {Date},
        language = {{{Language}}},
        keywords = {{autres, art-rapport-crh, crh-{Date}}}
        }}"""
            art_rapport_ls.append(art_rapport)  # reference to the original 1997 typology
            # un rapport non pris en compte = l'id #1901
    else:
        print(ID, Title)

intro_ls = intro_preface_ls + intro_book_ls + intro_journal_ls
preface_ls = preface_journal_ls + preface_book_ls
autres_ls = exposition_ls + report_ls + outil_ls + thesis_ls + conference_ls + art_rapport_ls
